package model;

import java.util.HashMap;

public class Dashboard1 {

	private String shieldName;
	private int refreshTime = 60000;
	private int rpmFan1Min = 0;
	private int rpmFan2Min = 0;
	private int rpmFan3Min = 0;
	private int rpmFan1Max = 1000;
	private int rpmFan2Max = 1000;
	private int rpmFan3Max = 1000;
	private int rpmPmpMin = 0;
	private int rpmPmpMax = 5000;
	private int tempIntMin = 0;
	private int tempIntMax = 100;
	private int tempExtMin = 0;
	private int tempExtMax = 100;
	private int pwrTX1Min = 0;
	private int pwrTX2Min = 0;
	private int pwrTX3Min = 0;
	private int pwrTX4Min = 0;
	private int pwrTX5Min = 0;
	private int pwrTX6Min = 0;
	private int pwrTX1Max = 1000;
	private int pwrTX2Max = 1000;
	private int pwrTX3Max = 1000;
	private int pwrTX4Max = 1000;
	private int pwrTX5Max = 1000;
	private int pwrTX6Max = 1000;
	private int rpmFan4Max = 1000;
	private int rpmFan4Min = 1000;
	private int rpmFanAirMax = 1000;
	private int rpmFanAirMin = 1000;

	public Dashboard1 (String shieldName, int refreshTime, int rpmFan1Min, int rpmFan2Min, int rpmFan3Min, int rpmFan1Max, int rpmFan2Max, int rpmFan3Max, int rpmPmpMin, int rpmPmpMax,
			int tempIntMin, int tempIntMax, int tempExtMin, int tempExtMax, int pwrTX1Min, int pwrTX2Min, int pwrTX3Min, int pwrTX4Min, int pwrTX5Min, int pwrTX6Min,
			int pwrTX1Max, int pwrTX2Max, int pwrTX3Max, int pwrTX4Max, int pwrTX5Max, int pwrTX6Max, int rpmFan4Max, int rpmFan4Min, int rpmFanAirMax, int rpmFanAirMin){

		this.setShieldName(shieldName);
		this.rpmFan1Min = rpmFan1Min;
		this.rpmFan2Min = rpmFan2Min;
		this.rpmFan3Min = rpmFan3Min;
		this.rpmFan1Max = rpmFan1Max;
		this.rpmFan2Max = rpmFan2Max;
		this.rpmFan3Max = rpmFan3Max;
		this.rpmPmpMin = rpmPmpMin;
		this.rpmPmpMax = rpmPmpMax;
		this.tempIntMin = tempIntMin;
		this.tempIntMax = tempIntMax;
		this.tempExtMin = tempExtMin;
		this.tempExtMax = tempExtMax;
		this.pwrTX1Min = pwrTX1Min;
		this.pwrTX2Min = pwrTX2Min;
		this.pwrTX3Min = pwrTX3Min;
		this.pwrTX4Min = pwrTX4Min;
		this.pwrTX5Min = pwrTX5Min;
		this.pwrTX6Min = pwrTX6Min;
		this.pwrTX1Max = pwrTX1Max;
		this.pwrTX2Max = pwrTX2Max;
		this.pwrTX3Max = pwrTX3Max;
		this.pwrTX4Max = pwrTX4Max;
		this.pwrTX5Max = pwrTX5Max;
		this.pwrTX6Max = pwrTX6Max;
		this.refreshTime = refreshTime;
		this.rpmFan4Max = rpmFan4Max;
		this.rpmFan4Min = rpmFan4Min;
		this.rpmFanAirMax = rpmFanAirMax;
		this.rpmFanAirMin = rpmFanAirMin;

	}

	public int getRpmFan4Max() {
		return rpmFan4Max;
	}

	public void setRpmFan4Max(int rpmFan4Max) {
		this.rpmFan4Max = rpmFan4Max;
	}

	public int getRpmFan4Min() {
		return rpmFan4Min;
	}

	public void setRpmFan4Min(int rpmFan4Min) {
		this.rpmFan4Min = rpmFan4Min;
	}

	public int getRpmFanAirMax() {
		return rpmFanAirMax;
	}

	public void setRpmFanAirMax(int rpmFanAirMax) {
		this.rpmFanAirMax = rpmFanAirMax;
	}

	public int getRpmFanAirMin() {
		return rpmFanAirMin;
	}

	public void setRpmFanAirMin(int rpmFanAirMin) {
		this.rpmFanAirMin = rpmFanAirMin;
	}

	/**
	 * @return the refreshTime
	 */
	public int getRefreshTime() {
		return refreshTime;
	}

	/**
	 * @return the rpmFan1Min
	 */
	public int getRpmFan1Min() {
		return rpmFan1Min;
	}

	/**
	 * @return the rpmFan2Min
	 */
	public int getRpmFan2Min() {
		return rpmFan2Min;
	}

	/**
	 * @return the rpmFan3Min
	 */
	public int getRpmFan3Min() {
		return rpmFan3Min;
	}

	/**
	 * @return the rpmFan1Max
	 */
	public int getRpmFan1Max() {
		return rpmFan1Max;
	}

	/**
	 * @return the rpmFan2Max
	 */
	public int getRpmFan2Max() {
		return rpmFan2Max;
	}

	/**
	 * @return the rpmFan3Max
	 */
	public int getRpmFan3Max() {
		return rpmFan3Max;
	}

	/**
	 * @return the rpmPmpMin
	 */
	public int getRpmPmpMin() {
		return rpmPmpMin;
	}

	/**
	 * @return the rpmPmpMax
	 */
	public int getRpmPmpMax() {
		return rpmPmpMax;
	}

	/**
	 * @return the tempIntMin
	 */
	public int getTempIntMin() {
		return tempIntMin;
	}

	/**
	 * @return the tempIntMax
	 */
	public int getTempIntMax() {
		return tempIntMax;
	}

	/**
	 * @return the tempExtMin
	 */
	public int getTempExtMin() {
		return tempExtMin;
	}

	/**
	 * @return the tempExtMax
	 */
	public int getTempExtMax() {
		return tempExtMax;
	}

	/**
	 * @return the pwrTX1Min
	 */
	public int getPwrTX1Min() {
		return pwrTX1Min;
	}

	/**
	 * @return the pwrTX2Min
	 */
	public int getPwrTX2Min() {
		return pwrTX2Min;
	}

	/**
	 * @return the pwrTX3Min
	 */
	public int getPwrTX3Min() {
		return pwrTX3Min;
	}

	/**
	 * @return the pwrTX4Min
	 */
	public int getPwrTX4Min() {
		return pwrTX4Min;
	}

	/**
	 * @return the pwrTX5Min
	 */
	public int getPwrTX5Min() {
		return pwrTX5Min;
	}

	/**
	 * @return the pwrTX6Min
	 */
	public int getPwrTX6Min() {
		return pwrTX6Min;
	}

	/**
	 * @return the pwrTX1Max
	 */
	public int getPwrTX1Max() {
		return pwrTX1Max;
	}

	/**
	 * @return the pwrTX2Max
	 */
	public int getPwrTX2Max() {
		return pwrTX2Max;
	}

	/**
	 * @return the pwrTX3Max
	 */
	public int getPwrTX3Max() {
		return pwrTX3Max;
	}

	/**
	 * @return the pwrTX4Max
	 */
	public int getPwrTX4Max() {
		return pwrTX4Max;
	}

	/**
	 * @return the pwrTX5Max
	 */
	public int getPwrTX5Max() {
		return pwrTX5Max;
	}

	/**
	 * @return the pwrTX6Max
	 */
	public int getPwrTX6Max() {
		return pwrTX6Max;
	}

	/**
	 * @param refreshTime the refreshTime to set
	 */
	public void setRefreshTime(int refreshTime) {
		this.refreshTime = refreshTime;
	}

	/**
	 * @param rpmFan1Min the rpmFan1Min to set
	 */
	public void setRpmFan1Min(int rpmFan1Min) {
		this.rpmFan1Min = rpmFan1Min;
	}

	/**
	 * @param rpmFan2Min the rpmFan2Min to set
	 */
	public void setRpmFan2Min(int rpmFan2Min) {
		this.rpmFan2Min = rpmFan2Min;
	}

	/**
	 * @param rpmFan3Min the rpmFan3Min to set
	 */
	public void setRpmFan3Min(int rpmFan3Min) {
		this.rpmFan3Min = rpmFan3Min;
	}

	/**
	 * @param rpmFan1Max the rpmFan1Max to set
	 */
	public void setRpmFan1Max(int rpmFan1Max) {
		this.rpmFan1Max = rpmFan1Max;
	}

	/**
	 * @param rpmFan2Max the rpmFan2Max to set
	 */
	public void setRpmFan2Max(int rpmFan2Max) {
		this.rpmFan2Max = rpmFan2Max;
	}

	/**
	 * @param rpmFan3Max the rpmFan3Max to set
	 */
	public void setRpmFan3Max(int rpmFan3Max) {
		this.rpmFan3Max = rpmFan3Max;
	}

	/**
	 * @param rpmPmpMin the rpmPmpMin to set
	 */
	public void setRpmPmpMin(int rpmPmpMin) {
		this.rpmPmpMin = rpmPmpMin;
	}

	/**
	 * @param rpmPmpMax the rpmPmpMax to set
	 */
	public void setRpmPmpMax(int rpmPmpMax) {
		this.rpmPmpMax = rpmPmpMax;
	}

	/**
	 * @param tempIntMin the tempIntMin to set
	 */
	public void setTempIntMin(int tempIntMin) {
		this.tempIntMin = tempIntMin;
	}

	/**
	 * @param tempIntMax the tempIntMax to set
	 */
	public void setTempIntMax(int tempIntMax) {
		this.tempIntMax = tempIntMax;
	}

	/**
	 * @param tempExtMin the tempExtMin to set
	 */
	public void setTempExtMin(int tempExtMin) {
		this.tempExtMin = tempExtMin;
	}

	/**
	 * @param tempExtMax the tempExtMax to set
	 */
	public void setTempExtMax(int tempExtMax) {
		this.tempExtMax = tempExtMax;
	}

	/**
	 * @param pwrTX1Min the pwrTX1Min to set
	 */
	public void setPwrTX1Min(int pwrTX1Min) {
		this.pwrTX1Min = pwrTX1Min;
	}

	/**
	 * @param pwrTX2Min the pwrTX2Min to set
	 */
	public void setPwrTX2Min(int pwrTX2Min) {
		this.pwrTX2Min = pwrTX2Min;
	}

	/**
	 * @param pwrTX3Min the pwrTX3Min to set
	 */
	public void setPwrTX3Min(int pwrTX3Min) {
		this.pwrTX3Min = pwrTX3Min;
	}

	/**
	 * @param pwrTX4Min the pwrTX4Min to set
	 */
	public void setPwrTX4Min(int pwrTX4Min) {
		this.pwrTX4Min = pwrTX4Min;
	}

	/**
	 * @param pwrTX5Min the pwrTX5Min to set
	 */
	public void setPwrTX5Min(int pwrTX5Min) {
		this.pwrTX5Min = pwrTX5Min;
	}

	/**
	 * @param pwrTX6Min the pwrTX6Min to set
	 */
	public void setPwrTX6Min(int pwrTX6Min) {
		this.pwrTX6Min = pwrTX6Min;
	}

	/**
	 * @param pwrTX1Max the pwrTX1Max to set
	 */
	public void setPwrTX1Max(int pwrTX1Max) {
		this.pwrTX1Max = pwrTX1Max;
	}

	/**
	 * @param pwrTX2Max the pwrTX2Max to set
	 */
	public void setPwrTX2Max(int pwrTX2Max) {
		this.pwrTX2Max = pwrTX2Max;
	}

	/**
	 * @param pwrTX3Max the pwrTX3Max to set
	 */
	public void setPwrTX3Max(int pwrTX3Max) {
		this.pwrTX3Max = pwrTX3Max;
	}

	/**
	 * @param pwrTX4Max the pwrTX4Max to set
	 */
	public void setPwrTX4Max(int pwrTX4Max) {
		this.pwrTX4Max = pwrTX4Max;
	}

	/**
	 * @param pwrTX5Max the pwrTX5Max to set
	 */
	public void setPwrTX5Max(int pwrTX5Max) {
		this.pwrTX5Max = pwrTX5Max;
	}

	/**
	 * @param pwrTX6Max the pwrTX6Max to set
	 */
	public void setPwrTX6Max(int pwrTX6Max) {
		this.pwrTX6Max = pwrTX6Max;
	}

	public String getShieldName() {
		return shieldName;
	}

	public void setShieldName(String shieldName) {
		this.shieldName = shieldName;
	}



}


