package model;

import java.util.HashMap;

public class Config {

	private String MQTTBrokerIp;
	private String MQTTBrokerPort;
	private String password;
	private HashMap shield;
	private int numberShiledInstalled;
	/**
	 * @Refresh Time expressed in milliseconds
	 */
	private int refreshTime = 60000;


	public Config (){
	}

	public Config (String MQTTBrookerIp, String MQTTBrokerPort, String password, HashMap shield, int numberShieldInstalled){

		this.MQTTBrokerIp= MQTTBrookerIp;
		this.MQTTBrokerPort = MQTTBrokerPort;
		this.shield = shield;
		this.numberShiledInstalled = numberShieldInstalled;
		this.password = password;

	}

	/**
	 * @return the mQTTBrokerIp
	 */
	public String getMQTTBrokerIp() {
		return MQTTBrokerIp;
	}

	/**
	 * @return the mQTTBrokerPort
	 */
	public String getMQTTBrokerPort() {
		return MQTTBrokerPort;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the shield
	 */
	public HashMap getShield() {
		return shield;
	}

	/**
	 * @return the numberShiledInstalled
	 */
	public int getNumberShiledInstalled() {
		return numberShiledInstalled;
	}

	/**
	 * @return the refreshTime
	 */
	public int getRefreshTime() {
		return refreshTime;
	}

	/**
	 * @param mQTTBrokerIp the mQTTBrokerIp to set
	 */
	public void setMQTTBrokerIp(String mQTTBrokerIp) {
		MQTTBrokerIp = mQTTBrokerIp;
	}

	/**
	 * @param mQTTBrokerPort the mQTTBrokerPort to set
	 */
	public void setMQTTBrokerPort(String mQTTBrokerPort) {
		MQTTBrokerPort = mQTTBrokerPort;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param shield the shield to set
	 */
	public void setShield(HashMap shield) {
		this.shield = shield;
	}

	/**
	 * @param numberShiledInstalled the numberShiledInstalled to set
	 */
	public void setNumberShiledInstalled(int numberShiledInstalled) {
		this.numberShiledInstalled = numberShiledInstalled;
	}

	/**
	 * @param refreshTime the refreshTime to set
	 */
	public void setRefreshTime(int refreshTime) {
		this.refreshTime = refreshTime;
	}



}


