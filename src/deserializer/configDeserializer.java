package deserializer;

import java.util.HashMap;

import application.PropertyFileHelper;
import model.Config;

public class configDeserializer {


	public static Config config(String fileName){

		String MQTTBrokerIp = PropertyFileHelper.getProperties(fileName, "MQTTBrokerIp");
		String MQTTBrokerPort = PropertyFileHelper.getProperties(fileName, "MQTTBrokerPort");
		String password = PropertyFileHelper.getProperties(fileName, "password");
		int numberShiledInstalled = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "numberShiledInstalled").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "numberShiledInstalled")) ;
		HashMap<String, String> shield = new HashMap<>();
		for (int i =0 ; i<numberShiledInstalled; i++){

			shield.put(String.valueOf(i),  PropertyFileHelper.getProperties("dash"+String.valueOf(i+1), "MQTTBrokerIp"));

		}
		Config config = new Config(MQTTBrokerIp, MQTTBrokerPort, password, shield, numberShiledInstalled);

		return config;

	}


}
