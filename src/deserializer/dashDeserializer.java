package deserializer;

import java.util.HashMap;

import application.PropertyFileHelper;
import model.Config;
import model.Dashboard1;

public class dashDeserializer {


	public static Dashboard1 dashboard(String fileName){

		String shieldName = PropertyFileHelper.getProperties(fileName, "shieldName");
		int refreshTime = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "refreshTime").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "refreshTime"));
		int rpmFanAirMin = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFanAirMin").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFanAirMin"));
		int rpmFan1Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan1Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan1Min"));
		int rpmFan2Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan2Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan2Min"));
		int rpmFan3Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan3Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan3Min"));
		int rpmFan4Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan4Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan4Min"));
		int rpmFanAirMax = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFanAirMax").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFanAirMax"));
		int rpmFan1Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan1Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan1Max"));
		int rpmFan2Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan2Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan2Max"));
		int rpmFan3Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan3Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan3Max"));
		int rpmFan4Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmFan4Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmFan4Max"));
		int rpmPmpMin = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmPmpMin").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmPmpMin"));
		int rpmPmpMax = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "rpmPmpMax").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "rpmPmpMax"));
		int tempIntMin = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "tempIntMin").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "tempIntMin"));
		int tempIntMax = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "tempIntMax").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "tempIntMax"));
		int tempExtMin = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "tempExtMin").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "tempExtMin"));
		int tempExtMax = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "tempExtMax").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "tempExtMax"));
		int pwrTX1Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX1Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX1Min"));
		int pwrTX2Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX2Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX2Min"));
		int pwrTX3Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX3Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX3Min"));
		int pwrTX4Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX4Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX4Min"));
		int pwrTX5Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX5Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX5Min"));
		int pwrTX6Min = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX6Min").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX6Min"));
		int pwrTX1Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX1Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX1Max"));
		int pwrTX2Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX2Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX2Max"));
		int pwrTX3Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX3Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX3Max"));
		int pwrTX4Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX4Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX4Max"));
		int pwrTX5Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX5Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX5Max"));
		int pwrTX6Max = Integer.valueOf(PropertyFileHelper.getProperties(fileName, "pwrTX6Max").compareTo("")==0 ? "0" : PropertyFileHelper.getProperties(fileName, "pwrTX6Max"));

		Dashboard1 dashboard = new Dashboard1(shieldName, refreshTime,rpmFan1Min, rpmFan2Min, rpmFan3Min, rpmFan1Max, rpmFan2Max, rpmFan3Max, rpmPmpMin, rpmPmpMax, tempIntMin, tempIntMax, tempExtMin, tempExtMax, pwrTX1Min, pwrTX2Min, pwrTX3Min, pwrTX4Min, pwrTX5Min, pwrTX6Min, pwrTX1Max, pwrTX2Max, pwrTX3Max, pwrTX4Max, pwrTX5Max, pwrTX6Max, rpmFan4Max, rpmFan4Min, rpmFanAirMax, rpmFanAirMin);

		return dashboard;

	}


}

