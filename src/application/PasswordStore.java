package application;



import java.io.*;
import java.lang.annotation.Documented;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import util.CryptoUtil;

public class PasswordStore {
    static private PasswordStore _instance = null;
    static private String key="supercalifragilistichespiralidoso";
    private static CryptoUtil cryptoUtil=new CryptoUtil();
	private static HashMap<String, String> props = new HashMap<>();
	private static String PATH = "src\\propertis\\users.prop";


    protected PasswordStore(){
    try{
//        InputStream file = new FileInputStream(new File("users.properties")) ;
//        props = new Properties();
//        props.load(file);

        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				String user = sCurrentLine;
				String pwd = br.readLine();
				props.put(user, pwd);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
//        port = props.getProperty("PORT");
//        ip = props.getProperty("IP");
//        database = props.getProperty("DATABASE");
//        user = props.getProperty("USER");
//        pass = props.getProperty("PASS");
//        jdbc = props.getProperty("JDBC");
//        driver = props.getProperty("DRIVERNAME");
//        instance = props.getProperty("INSTANCE");
       }
    catch(Exception e){
        System.out.println("error" + e);
       }
    }

    static public PasswordStore instance(){
        if (_instance == null) {
            _instance = new PasswordStore();
        }
        return _instance;
    }

    static public boolean login(String user, String pwd){
    	if(getUserPwd(user).equals(pwd))
    		return true;
    	else
    		return false;
    }

    static public String getUserPwd(String user){

    	String usrEnc="";
    	String pwd="";

    	try {
			usrEnc = cryptoUtil.encrypt(key, user);
			String pwdEnc = props.get(usrEnc);
	    	pwd=cryptoUtil.decrypt(key, pwdEnc);

		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



    	return pwd;
    }

    static public void setUserPwd(String user, String pwd){
    	InputStream file;
		try {
			file = new FileInputStream(new File(PATH));
			Properties props = new Properties();
	        props.load(file);
	        String usrEnc=cryptoUtil.encrypt(key, user);
	        String pwdEnc=cryptoUtil.encrypt(key, pwd);

	        props.setProperty(usrEnc,pwdEnc);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

public void setNewPassword(String currUser, String newPass) {
		
		try {
			String userEnc = cryptoUtil.encrypt(key, currUser);
			String pwdEnc  = cryptoUtil.encrypt(key, newPass);
			props.put(userEnc, pwdEnc);
			
			PrintWriter out = new PrintWriter(PATH);
			
			
			
			Iterator it = props.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println(pair.getKey() + " = " + pair.getValue());
		        
		        out.println(pair.getKey() );
		        out.println(pair.getValue() );
		        it.remove(); // avoids a ConcurrentModificationException
		    }
			
		    out.close();
			
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
 catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}