package application;

import eu.hansolo.medusa.Gauge;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;

/**
 * View-Controller for the person table.
 *
 * @author Marco Jakob
 */
public class EventHandlingController {

	@FXML
	private Gauge GaugeTemInt;
	@FXML
	private Gauge GaugeTemExt;
	@FXML
	private Gauge GaugeTempMonitor1;
	@FXML
	private Gauge GaugeTempMonitor2;
	@FXML
	private Gauge GaugeTempMonitor3;
	@FXML
	private Gauge GaugeTempMonitor4;
	@FXML
	private Gauge GaugeTempMonitor5;
	@FXML
	private Gauge GaugeTempMonitor6;
	@FXML
	private Gauge GaugePowerMonitor1;
	@FXML
	private Gauge GaugePowerMonitor2;
	@FXML
	private Gauge GaugePowerMonitor3;
	@FXML
	private Gauge GaugePowerMonitor4;
	@FXML
	private Gauge GaugePowerMonitor5;
	@FXML
	private Gauge GaugePowerMonitor6;
	@FXML
	private Gauge GaugeLCDAlim1;
	@FXML
	private Gauge GaugeLCDAlim2;
	@FXML
	private Gauge GaugeLCDAlim3;
	@FXML
	private Gauge GaugeWaterPump;
	@FXML
	private Gauge GaugeFanWaterPump1;
	@FXML
	private Gauge GaugeFanWaterPump2;
	@FXML
	private Gauge GaugeFanWaterPump3;
	@FXML
	private Slider SliderFan1;
	@FXML
	private Slider SliderFan2;
	@FXML
	private Slider SliderFan3;
	@FXML
	private Slider SliderFan666;

	@FXML
	private ComboBox<Person> myComboBox;

	@FXML
	private ObservableList<Person> myComboBoxData = FXCollections.observableArrayList();

	@FXML
	private Hyperlink myHyperlink;

	@FXML
	private Slider mySlider;

	@FXML
	private TextField myTextField;

	@FXML
	private ListView<Person> myListView;
	private ObservableList<Person> listViewData = FXCollections.observableArrayList();

	@FXML
	private TextArea outputTextArea;


	/**
	 * The constructor (is called before the initialize()-method).
	 */
	public EventHandlingController() {
		// Create some sample data for the ComboBox and ListView.
		GaugeLCDAlim1 = new Gauge();
		GaugeLCDAlim2 = new Gauge();
		GaugeLCDAlim3 = new Gauge();
		GaugeFanWaterPump1 = new Gauge();
		GaugeFanWaterPump2 = new Gauge();
		GaugeFanWaterPump3 = new Gauge();
		GaugePowerMonitor1 = new Gauge();
		GaugePowerMonitor2 = new Gauge();
		GaugePowerMonitor3 = new Gauge();
		GaugePowerMonitor4 = new Gauge();
		GaugePowerMonitor5 = new Gauge();
		GaugePowerMonitor6 = new Gauge();
		GaugeTemExt = new Gauge();
		GaugeTemInt = new Gauge();
		GaugeTempMonitor1 = new Gauge();
		GaugeTempMonitor2 = new Gauge();
		GaugeTempMonitor3 = new Gauge();
		GaugeTempMonitor4 = new Gauge();
		GaugeTempMonitor5 = new Gauge();
		GaugeTempMonitor6 = new Gauge();
		GaugeWaterPump = new Gauge();
		SliderFan1 = new Slider();
		SliderFan2 = new Slider();
		SliderFan3 = new Slider();

	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		GaugeLCDAlim1.setValue(2.90);
		GaugeLCDAlim2.setValue(12.40);
		GaugeLCDAlim3.setValue(15.85);
		GaugeFanWaterPump1.setValue(0.00);
		GaugeFanWaterPump2.setValue(75.85);
		GaugeFanWaterPump3.setValue(98.85);
		GaugePowerMonitor1.setValue(70.85);
		GaugePowerMonitor2.setValue(55.85);
		GaugePowerMonitor3.setValue(34.85);
		GaugePowerMonitor4.setValue(76.85);
		GaugePowerMonitor5.setValue(54.85);
		GaugePowerMonitor6.setValue(99.85);
		GaugeTemExt.setValue(15.85);
		GaugeTemInt.setValue(25.85);
		GaugeTempMonitor1.setValue(15.85);
		GaugeTempMonitor2.setValue(17.85);
		GaugeTempMonitor3.setValue(30.85);
		GaugeTempMonitor4.setValue(12.85);
		GaugeTempMonitor5.setValue(45.85);
		GaugeTempMonitor6.setValue(80.85);
		GaugeWaterPump.setValue(68.85);
		GaugeWaterPump.setLedOn(true);
		GaugeWaterPump.setMediumTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMajorTickMarkColor(Color.BLUE);
		GaugeFanWaterPump1.setMediumTickMarkColor(Color.BLUE);
		GaugeFanWaterPump1.setMajorTickMarkColor(Color.BLUE);
		GaugeFanWaterPump2.setMediumTickMarkColor(Color.BLUE);
		GaugeFanWaterPump2.setMajorTickMarkColor(Color.BLUE);
		GaugeFanWaterPump3.setMediumTickMarkColor(Color.BLUE);
		GaugeFanWaterPump3.setMajorTickMarkColor(Color.BLUE);
		GaugePowerMonitor1.setMediumTickMarkColor(Color.BLUE);
		GaugePowerMonitor1.setMajorTickMarkColor(Color.BLUE);
		GaugePowerMonitor2.setMediumTickMarkColor(Color.BLUE);
		GaugePowerMonitor2.setMajorTickMarkColor(Color.BLUE);
		GaugePowerMonitor3.setMediumTickMarkColor(Color.BLUE);
		GaugePowerMonitor3.setMajorTickMarkColor(Color.BLUE);
		GaugePowerMonitor4.setMediumTickMarkColor(Color.BLUE);
		GaugePowerMonitor4.setMajorTickMarkColor(Color.BLUE);
		GaugePowerMonitor5.setMediumTickMarkColor(Color.BLUE);
		GaugePowerMonitor5.setMajorTickMarkColor(Color.BLUE);
		GaugePowerMonitor6.setMediumTickMarkColor(Color.BLUE);
		GaugePowerMonitor6.setMajorTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMediumTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMajorTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMediumTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMajorTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMediumTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMajorTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMediumTickMarkColor(Color.BLUE);
		GaugeWaterPump.setMajorTickMarkColor(Color.BLUE);
		GaugeTemExt.setMediumTickMarkColor(Color.BLUE);
		GaugeTemExt.setMajorTickMarkColor(Color.BLUE);
		GaugeTemInt.setMediumTickMarkColor(Color.BLUE);
		GaugeTemInt.setMajorTickMarkColor(Color.BLUE);
		GaugeTempMonitor1.setMediumTickMarkColor(Color.BLUE);
		GaugeTempMonitor1.setMajorTickMarkColor(Color.BLUE);
		GaugeTempMonitor2.setMediumTickMarkColor(Color.BLUE);
		GaugeTempMonitor2.setMajorTickMarkColor(Color.BLUE);
		GaugeTempMonitor3.setMediumTickMarkColor(Color.BLUE);
		GaugeTempMonitor3.setMajorTickMarkColor(Color.BLUE);
		GaugeTempMonitor4.setMediumTickMarkColor(Color.BLUE);
		GaugeTempMonitor4.setMajorTickMarkColor(Color.BLUE);
		GaugeTempMonitor5.setMediumTickMarkColor(Color.BLUE);
		GaugeTempMonitor5.setMajorTickMarkColor(Color.BLUE);
		GaugeTempMonitor6.setMediumTickMarkColor(Color.BLUE);
		GaugeTempMonitor6.setMajorTickMarkColor(Color.BLUE);


		// Handle Button event.
		SliderFan1.setOnDragDetected((event) -> {
			GaugeFanWaterPump1.setValue(SliderFan1.getValue());
		});
		SliderFan2.setOnDragDetected((event) -> {
			GaugeFanWaterPump2.setValue(SliderFan2.getValue());
		});
		SliderFan3.setOnDragDetected((event) -> {
			GaugeFanWaterPump3.setValue(SliderFan3.getValue());
		});

		// Handle CheckBox event.
		//myCheckBox.setOnAction((event) -> {
		//	boolean selected = myCheckBox.isSelected();
		//	outputTextArea.appendText("CheckBox Action (selected: " + selected + ")\n");
		//});

		// Init ComboBox items.
		//myComboBox.setItems(myComboBoxData);

		// Define rendering of the list of values in ComboBox drop down.
		//myComboBox.setCellFactory((comboBox) -> {
		/*	return new ListCell<Person>() {
				@Override
				protected void updateItem(Person item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
					} else {
						setText(item.getFirstName() + " " + item.getLastName());
					}
				}
			};
		});
		*/
		// Define rendering of selected value shown in ComboBox.
		/*myComboBox.setConverter(new StringConverter<Person>() {
			@Override
			public String toString(Person person) {
				if (person == null) {
					return null;
				} else {
					return person.getFirstName() + " " + person.getLastName();
				}
			}

			@Override
			public Person fromString(String personString) {
				return null; // No conversion fromString needed.
			}
		});
*/
		// Handle ComboBox event.
	/*	myComboBox.setOnAction((event) -> {
			Person selectedPerson = myComboBox.getSelectionModel().getSelectedItem();
			outputTextArea.appendText("ComboBox Action (selected: " + selectedPerson.toString() + ")\n");
		});

		// Handle Hyperlink event.
		myHyperlink.setOnAction((event) -> {
			outputTextArea.appendText("Hyperlink Action\n");
		});

		// Init ListView.
		myListView.setItems(listViewData);
		myListView.setCellFactory((list) -> {
			return new ListCell<Person>() {
				@Override
				protected void updateItem(Person item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
					} else {
						setText(item.getFirstName() + " " + item.getLastName());
					}
				}
			};
		});

		// Handle ListView selection changes.
		myListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			outputTextArea.appendText("ListView Selection Changed (selected: " + newValue.toString() + ")\n");
		});

		// Handle Slider value change events.
		mySlider.valueProperty().addListener((observable, oldValue, newValue) -> {
			outputTextArea.appendText("Slider Value Changed (newValue: " + newValue.intValue() + ")\n");
		});

		// Handle TextField text changes.
		//myTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			//outputTextArea.appendText("TextField Text Changed (newValue: " + newValue + ")\n");
		//});

		// Handle TextField enter key event.
		//myTextField.setOnAction((event) -> {
			//outputTextArea.appendText("TextField Action\n");
		//});
		*/
	}


}