package application;


import java.util.HashMap;

import javax.security.auth.Subject;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import eu.hansolo.medusa.Gauge;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public class SimpleMqttCallBack implements MqttCallback {

	private HashMap<String, Object> gauge;

	  public SimpleMqttCallBack(HashMap<String, Object> mContext) {
		gauge=mContext;


	}

	public void connectionLost(Throwable throwable) {
	    System.out.println("Connection to MQTT broker lost!");
	    DashController.dashboardStart();
	  }

	  public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
		try{
		  String key = s.substring(s.indexOf("/")+1,s.length());
		if(key.contains("frequency")){
			String msg = new String(mqttMessage.getPayload());
	        Double value = Double.valueOf(msg.substring(msg.indexOf("/")+1, msg.length()));
	        Text t = (Text) gauge.get(key);
	        t.setText("FREQ: "+String.valueOf(value.intValue())+" Mhz");
		}  
		else if(!key.contains("cmd/")){
			System.out.println("Message received:\t"+ new String(mqttMessage.getPayload()) );
			String msg = new String(mqttMessage.getPayload());
	        Double value = Double.valueOf(msg.substring(msg.indexOf("/")+1, msg.length()));
		    //gauge.getGauge().setValue(10.00);;
		    System.out.println("Ricevo: "+key+" - "+value.toString());
	        if(key.contains("alarm")){
	        	if(msg.compareTo("none")==0){
			    	if(msg.contains("dx")){
				    	Circle c = (Circle) gauge.get("dx/alarm/temp");
				    	c.setFill(Paint.valueOf("#000000"));
				    	Circle c1 = (Circle) gauge.get("dx/alarm/fan");
				    	c1.setFill(Paint.valueOf("#000000"));
				    	Circle c2 = (Circle) gauge.get("dx/alarm/tcase");
				    	c2.setFill(Paint.valueOf("#000000"));
				    	Circle c3 = (Circle) gauge.get("dx/alarm/vswr");
				    	c3.setFill(Paint.valueOf("#000000"));
				    	Circle c4 = (Circle) gauge.get("dx/alarm/voltage");
				    	c4.setFill(Paint.valueOf("#000000"));
				    	Circle c5 = (Circle) gauge.get("dx/alarm/general");
				    	c5.setFill(Paint.valueOf("#000000"));
			    	}else{
			    		Circle c = (Circle) gauge.get("sx/alarm/temp");
				    	c.setFill(Paint.valueOf("#000000"));
				    	Circle c1 = (Circle) gauge.get("sx/alarm/fan");
				    	c1.setFill(Paint.valueOf("#000000"));
				    	Circle c2 = (Circle) gauge.get("sx/alarm/tcase");
				    	c2.setFill(Paint.valueOf("#000000"));
				    	Circle c3 = (Circle) gauge.get("sx/alarm/vswr");
				    	c3.setFill(Paint.valueOf("#000000"));
				    	Circle c4 = (Circle) gauge.get("sx/alarm/voltage");
				    	c4.setFill(Paint.valueOf("#000000"));
				    	Circle c5 = (Circle) gauge.get("sx/alarm/general");
				    	c5.setFill(Paint.valueOf("#000000"));
			    	}
			    }else{
					Circle g = (Circle) gauge.get(key);
				    	g.setFill(Paint.valueOf("#dd4b39"));
			    }
	        }else{
			    Gauge g = (Gauge) gauge.get(key);
			    if(g!=null)
			    g.setValue(value);
	        }
	    }
		}catch (NumberFormatException e) {
			String key = s.substring(s.indexOf("/")+1,s.length());
			System.out.println("Message received:\t"+ new String(mqttMessage.getPayload()) );
			String msg = new String(mqttMessage.getPayload());
	        //Double value = Double.valueOf(msg.substring(msg.indexOf("/")+1, msg.length()));
		    //gauge.getGauge().setValue(10.00);;
		    //System.out.println(new String(mqttMessage.getPayload()));
			if(!msg.contains("stopped")&&!msg.contains("running") && !key.contains("status")){
			    if(msg.compareTo("none")==0){
			    	if(msg.contains("dx")){
				    	Circle c = (Circle) gauge.get("dx/alarm/temp");
				    	c.setFill(Paint.valueOf("#000000"));
				    	Circle c1 = (Circle) gauge.get("dx/alarm/fan");
				    	c1.setFill(Paint.valueOf("#000000"));
				    	Circle c2 = (Circle) gauge.get("dx/alarm/tcase");
				    	c2.setFill(Paint.valueOf("#000000"));
				    	Circle c3 = (Circle) gauge.get("dx/alarm/vswr");
				    	c3.setFill(Paint.valueOf("#000000"));
				    	Circle c4 = (Circle) gauge.get("dx/alarm/voltage");
				    	c4.setFill(Paint.valueOf("#000000"));
				    	Circle c5 = (Circle) gauge.get("dx/alarm/general");
				    	c5.setFill(Paint.valueOf("#000000"));
			    	}else{
			    		Circle c = (Circle) gauge.get("sx/alarm/temp");
				    	c.setFill(Paint.valueOf("#000000"));
				    	Circle c1 = (Circle) gauge.get("sx/alarm/fan");
				    	c1.setFill(Paint.valueOf("#000000"));
				    	Circle c2 = (Circle) gauge.get("sx/alarm/tcase");
				    	c2.setFill(Paint.valueOf("#000000"));
				    	Circle c3 = (Circle) gauge.get("sx/alarm/vswr");
				    	c3.setFill(Paint.valueOf("#000000"));
				    	Circle c4 = (Circle) gauge.get("sx/alarm/voltage");
				    	c4.setFill(Paint.valueOf("#000000"));
				    	Circle c5 = (Circle) gauge.get("sx/alarm/general");
				    	c5.setFill(Paint.valueOf("#000000"));
			    	}
			    }else{
					Circle g = (Circle) gauge.get(key);
					if(g!=null && (msg.length()>7 || msg.compareTo("on")==0))
				    	g.setFill(Paint.valueOf("#dd4b39"));
					else if(g!=null)
						g.setFill(Paint.valueOf("#000000"));
					
			    }
			}
		}
	    


	  }

	  

	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
	  }
	}
