
package application;

import java.io.IOException;
import java.util.HashMap;

import deserializer.dashDeserializer;
import eu.hansolo.medusa.Gauge;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Dashboard1;


/**
 * View-Controller for the person table.
 *
 * @author Marco Jakob
 */
public class DashController {

	private Main mContext;
	


	@FXML
	private void menuItemShowDash(ActionEvent event) {
		tabPane.getTabs().remove(tabConfig);
		tabPane.getTabs().add(0,tabDash);
		tabPane.getTabs().add(1,tabDash2);

	}
	
	
	
	@FXML
	private void menuItemExit(ActionEvent event) {
		
		int result = MessageReceiver.stopClient();
		System.exit(result);

	}
	
	@FXML
	private void menuItemShowConfig(ActionEvent event) {

		tabPane.getTabs().remove(tabDash);
		tabPane.getTabs().remove(tabDash2);
		tabPane.getTabs().add(0,tabConfig);
	}

	@FXML
	private void menuItemChangePwd(ActionEvent event) {

		mContext.showChangePassDialog();
	}

	@FXML
	private TabPane tabPane = new TabPane();
	@FXML
	private Tab tabDash = new Tab();
	@FXML
	private Tab tabDash2 = new Tab();
	@FXML
	private Tab tabConfig = new Tab();
	@FXML
	private MenuItem menuConfig = new MenuItem();
	@FXML
	private MenuItem menuDash = new MenuItem();
	@FXML
	private MenuItem menuPwd = new MenuItem();
	@FXML
	private MenuItem menuExit = new MenuItem();

	
	
	@FXML
	private Gauge GaugeTemInt;
	@FXML
	private Gauge GaugeTemWater;
	@FXML
	private Gauge GaugePowerMonitor1;
	@FXML
	private Gauge GaugePowerMonitor2;
	@FXML
	private Gauge GaugePowerMonitor3;
	@FXML
	private Gauge GaugePowerMonitor4;
	@FXML
	private Gauge GaugePowerMonitor5;
	@FXML
	private Gauge GaugePowerMonitor6;
	@FXML
	private Gauge GaugeTempMonitor1;
	@FXML
	private Gauge GaugeTempMonitor2;
	@FXML
	private Gauge GaugeTempMonitor3;
	@FXML
	private Gauge GaugeTempMonitor4;
	@FXML
	private Gauge GaugeTempMonitor5;
	@FXML
	private Gauge GaugeTempMonitor6;
	@FXML
	private Gauge GaugeLCDAlim1;
	@FXML
	private Gauge GaugeLCDAlim2;
	@FXML
	private Gauge GaugeLCDAlim3;
	@FXML
	private Gauge GaugeWaterPump;
	@FXML
	private Gauge GaugeFanAir;
	@FXML
	private Gauge GaugeFanWaterPump1;
	@FXML
	private Gauge GaugeFanWaterPump2;
	@FXML
	private Gauge GaugeFanWaterPump3;
	@FXML
	private Gauge GaugeFanWaterPump4;
	@FXML
	private Slider SliderSwitchAmpl1;
	private Boolean stateSwitchAmpl1 = false;
	@FXML
	private Slider SliderSwitchAmpl2;
	private Boolean stateSwitchAmpl2 = false;
	@FXML
	private Slider SliderSwitchAmpl3;
	private Boolean stateSwitchAmpl3 = false;
	@FXML
	private Slider SliderSwitchAmpl4;
	private Boolean stateSwitchAmpl4 = false;
	@FXML
	private Slider SliderSwitchAmpl5;
	private Boolean stateSwitchAmpl5 = false;
	@FXML
	private Slider SliderSwitchAmpl6;
	private Boolean stateSwitchAmpl6 = false;
	@FXML
	private Circle AlarmVSWR1;
	@FXML
	private Circle AlarmVSWR2;
	@FXML
	private Circle AlarmVSWR3;
	@FXML
	private Circle AlarmVSWR4;
	@FXML
	private Circle AlarmVSWR5;
	@FXML
	private Circle AlarmVSWR6;
	@FXML
	private Circle VSWR1Status;
	@FXML
	private Circle VSWR2Status;
	@FXML
	private Circle VSWR3Status;
	@FXML
	private Circle VSWR4Status;
	@FXML
	private Circle VSWR5Status;
	@FXML
	private Circle VSWR6Status;
	
	@FXML
	private Text textModuleFreq1;
	@FXML
	private Text textModuleFreq2;
	@FXML
	private Text textModuleFreq3;
	@FXML
	private Text textModuleFreq4;
	@FXML
	private Text textModuleFreq5;
	@FXML
	private Text textModuleFreq6;
	

	@FXML
	private Gauge D2GaugeTemInt;
	@FXML
	private Gauge D2GaugeTemWater;
	@FXML
	private Gauge D2GaugePowerMonitor1;
	@FXML
	private Gauge D2GaugePowerMonitor2;
	@FXML
	private Gauge D2GaugePowerMonitor3;
	@FXML
	private Gauge D2GaugePowerMonitor4;
	@FXML
	private Gauge D2GaugePowerMonitor5;
	@FXML
	private Gauge D2GaugePowerMonitor6;
	@FXML
	private Gauge D2GaugeTempMonitor1;
	@FXML
	private Gauge D2GaugeTempMonitor2;
	@FXML
	private Gauge D2GaugeTempMonitor3;
	@FXML
	private Gauge D2GaugeTempMonitor4;
	@FXML
	private Gauge D2GaugeTempMonitor5;
	@FXML
	private Gauge D2GaugeTempMonitor6;
	@FXML
	private Gauge D2GaugeLCDAlim1;
	@FXML
	private Gauge D2GaugeLCDAlim2;
	@FXML
	private Gauge D2GaugeLCDAlim3;
	@FXML
	private Gauge D2GaugeWaterPump;
	@FXML
	private Gauge D2GaugeFanAir;
	@FXML
	private Gauge D2GaugeFanWaterPump1;
	@FXML
	private Gauge D2GaugeFanWaterPump2;
	@FXML
	private Gauge D2GaugeFanWaterPump3;
	@FXML
	private Gauge D2GaugeFanWaterPump4;
	@FXML
	private Slider D2SliderSwitchAmpl1;
	private Boolean D2stateSwitchAmpl1 = false;
	@FXML
	private Slider D2SliderSwitchAmpl2;
	private Boolean D2stateSwitchAmpl2 = false;
	@FXML
	private Slider D2SliderSwitchAmpl3;
	private Boolean D2stateSwitchAmpl3 = false;
	@FXML
	private Slider D2SliderSwitchAmpl4;
	private Boolean D2stateSwitchAmpl4 = false;
	@FXML
	private Slider D2SliderSwitchAmpl5;
	private Boolean D2stateSwitchAmpl5 = false;
	@FXML
	private Slider D2SliderSwitchAmpl6;
	private Boolean D2stateSwitchAmpl6 = false;
	@FXML
	private Circle D2AlarmVSWR1;
	@FXML
	private Circle D2AlarmVSWR2;
	@FXML
	private Circle D2AlarmVSWR3;
	@FXML
	private Circle D2AlarmVSWR4;
	@FXML
	private Circle D2AlarmVSWR5;
	@FXML
	private Circle D2AlarmVSWR6;
	@FXML
	private Circle D2VSWR1Status;
	@FXML
	private Circle D2VSWR2Status;
	@FXML
	private Circle D2VSWR3Status;
	@FXML
	private Circle D2VSWR4Status;
	@FXML
	private Circle D2VSWR5Status;
	@FXML
	private Circle D2VSWR6Status;

	@FXML
	private Text D2textModuleFreq1;
	@FXML
	private Text D2textModuleFreq2;
	@FXML
	private Text D2textModuleFreq3;
	@FXML
	private Text D2textModuleFreq4;
	@FXML
	private Text D2textModuleFreq5;
	@FXML
	private Text D2textModuleFreq6;
	
	private static Dashboard1 dashboard01DX;
	private static Dashboard1 dashboard01SX;
	private static HashMap<String, Object> hmap;
	private static MessageReceiver dash1;
	/**
	 * The constructor (is called before the initialize()-method).
	 */
	public DashController() {

	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		hmap = new HashMap<String, Object>();
		
		hmap.put("dx/rfans/status", new Gauge());
		hmap.put("dx/rfan/1/speed", GaugeFanWaterPump1);
		hmap.put("dx/rfan/2/speed", GaugeFanWaterPump2);
		hmap.put("dx/rfan/3/speed", GaugeFanWaterPump3);
		hmap.put("dx/rfan/4/speed", GaugeFanWaterPump4);
		hmap.put("dx/fan/speed", GaugeFanAir);
		
		hmap.put("dx/pump/status", new Gauge());
		hmap.put("dx/pump/speed", GaugeWaterPump);
		hmap.put("dx/air_temp", GaugeTemInt);
		hmap.put("dx/wb_temp", GaugeTemWater);
		hmap.put("dx/3V3", GaugeLCDAlim1);
		hmap.put("dx/12V", GaugeLCDAlim2);
		hmap.put("dx/15V", GaugeLCDAlim3);
		
		hmap.put("dx/module/1/status", new Gauge());
		hmap.put("dx/module/2/status", new Gauge());
		hmap.put("dx/module/3/status", new Gauge());
		hmap.put("dx/module/4/status", new Gauge());
		hmap.put("dx/module/5/status", new Gauge());
		hmap.put("dx/module/6/status", new Gauge());
		
		hmap.put("dx/module/1/frequency", textModuleFreq1);
		hmap.put("dx/module/2/frequency", textModuleFreq2);
		hmap.put("dx/module/3/frequency", textModuleFreq3);
		hmap.put("dx/module/4/frequency", textModuleFreq4);
		hmap.put("dx/module/5/frequency", textModuleFreq5);
		hmap.put("dx/module/6/frequency", textModuleFreq6);
		
		
		hmap.put("dx/module/1/power", GaugePowerMonitor1);
		hmap.put("dx/module/2/power", GaugePowerMonitor2);
		hmap.put("dx/module/3/power", GaugePowerMonitor3);
		hmap.put("dx/module/4/power", GaugePowerMonitor4);
		hmap.put("dx/module/5/power", GaugePowerMonitor5);
		hmap.put("dx/module/6/power", GaugePowerMonitor6);
		
		hmap.put("dx/module/1/case", GaugeTempMonitor1);
		hmap.put("dx/module/2/case", GaugeTempMonitor2);
		hmap.put("dx/module/3/case", GaugeTempMonitor3);
		hmap.put("dx/module/4/case", GaugeTempMonitor4);
		hmap.put("dx/module/5/case", GaugeTempMonitor5);
		hmap.put("dx/module/6/case", GaugeTempMonitor6);
		
		
		hmap.put("dx/module/1/vswr", VSWR1Status);
		hmap.put("dx/module/2/vswr", VSWR2Status);
		hmap.put("dx/module/3/vswr", VSWR3Status);
		hmap.put("dx/module/4/vswr", VSWR4Status);
		hmap.put("dx/module/5/vswr", VSWR5Status);
		hmap.put("dx/module/6/vswr", VSWR6Status);
		
		hmap.put("dx/alarm/temp", AlarmVSWR1);
		hmap.put("dx/alarm/fan", AlarmVSWR2);
		hmap.put("dx/alarm/case", AlarmVSWR3);
		hmap.put("dx/alarm/vswr", AlarmVSWR4);
		hmap.put("dx/alarm/voltage", AlarmVSWR5);
		hmap.put("dx/alarm/general", AlarmVSWR6);
		
		hmap.put("sx/rfans/status", new Gauge());
		hmap.put("sx/rfan/1/speed", D2GaugeFanWaterPump1);
		hmap.put("sx/rfan/2/speed", D2GaugeFanWaterPump2);
		hmap.put("sx/rfan/3/speed", D2GaugeFanWaterPump3);
		hmap.put("sx/rfan/4/speed", D2GaugeFanWaterPump4);
		hmap.put("sx/fan/speed", D2GaugeFanAir);
		hmap.put("sx/pump/status", new Gauge());
		hmap.put("sx/pump/speed", D2GaugeWaterPump);
		hmap.put("sx/air_temp", D2GaugeTemInt);
		hmap.put("sx/wb_temp", D2GaugeTemWater);
		hmap.put("sx/3V3", D2GaugeLCDAlim1);
		hmap.put("sx/12V", D2GaugeLCDAlim2);
		hmap.put("sx/15V", D2GaugeLCDAlim3);
		
		hmap.put("sx/module/1/status", new Gauge());
		hmap.put("sx/module/2/status", new Gauge());
		hmap.put("sx/module/3/status", new Gauge());
		hmap.put("sx/module/4/status", new Gauge());
		hmap.put("sx/module/5/status", new Gauge());
		hmap.put("sx/module/6/status", new Gauge());
		
		hmap.put("sx/module/1/frequency", D2textModuleFreq1);
		hmap.put("sx/module/2/frequency", D2textModuleFreq2);
		hmap.put("sx/module/3/frequency", D2textModuleFreq3);
		hmap.put("sx/module/4/frequency", D2textModuleFreq4);
		hmap.put("sx/module/5/frequency", D2textModuleFreq5);
		hmap.put("sx/module/6/frequency", D2textModuleFreq6);
		
		
		hmap.put("sx/module/1/power", D2GaugePowerMonitor1);
		hmap.put("sx/module/2/power", D2GaugePowerMonitor2);
		hmap.put("sx/module/3/power", D2GaugePowerMonitor3);
		hmap.put("sx/module/4/power", D2GaugePowerMonitor4);
		hmap.put("sx/module/5/power", D2GaugePowerMonitor5);
		hmap.put("sx/module/6/power", D2GaugePowerMonitor6);
		
		hmap.put("sx/module/1/case", D2GaugeTempMonitor1);
		hmap.put("sx/module/2/case", D2GaugeTempMonitor2);
		hmap.put("sx/module/3/case", D2GaugeTempMonitor3);
		hmap.put("sx/module/4/case", D2GaugeTempMonitor4);
		hmap.put("sx/module/5/case", D2GaugeTempMonitor5);
		hmap.put("sx/module/6/case", D2GaugeTempMonitor6);

		hmap.put("sx/module/1/vswr", D2VSWR1Status);
		hmap.put("sx/module/2/vswr", D2VSWR2Status);
		hmap.put("sx/module/3/vswr", D2VSWR3Status);
		hmap.put("sx/module/4/vswr", D2VSWR4Status);
		hmap.put("sx/module/5/vswr", D2VSWR5Status);
		hmap.put("sx/module/6/vswr", D2VSWR6Status);

		hmap.put("sx/alarm/temp", D2AlarmVSWR1);
		hmap.put("sx/alarm/fan", D2AlarmVSWR2);
		hmap.put("sx/alarm/case", D2AlarmVSWR3);
		hmap.put("sx/alarm/vswr", D2AlarmVSWR4);
		hmap.put("sx/alarm/voltage", D2AlarmVSWR5);
		hmap.put("sx/alarm/general", D2AlarmVSWR6);
		
		
		dashboard01DX = dashDeserializer.dashboard("dash1");
		dashboard01SX = dashDeserializer.dashboard("dash2");
		
		tabPane.getTabs().remove(tabConfig);

		setColorGauge();
		setListner();
		setdashboard01DXMinMax();
		setdashboard01SXMinMax();
		
		dashboardStart();
		
		
	}

	public static void dashboardStart() {
		// TODO Auto-generated method stub
		dash1 = new MessageReceiver(dashboard01SX.getShieldName()+"/#", hmap);
		dash1.run();
		MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/sys", String.valueOf(dashboard01DX.getRefreshTime()));
		MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/sys", String.valueOf(dashboard01SX.getRefreshTime()));
		

	}
	private void setdashboard01DXMinMax() {

		GaugeWaterPump.setMaxValue(dashboard01DX.getRpmPmpMax());
		GaugeWaterPump.setMinValue(dashboard01DX.getRpmPmpMin());
		GaugeFanWaterPump1.setMaxValue(dashboard01DX.getRpmFan1Max());
		GaugeFanWaterPump1.setMinValue(dashboard01DX.getRpmFan1Min());
		GaugeFanWaterPump2.setMaxValue(dashboard01DX.getRpmFan2Max());
		GaugeFanWaterPump2.setMinValue(dashboard01DX.getRpmFan2Min());
		GaugeFanWaterPump3.setMaxValue(dashboard01DX.getRpmFan3Max());
		GaugeFanWaterPump3.setMinValue(dashboard01DX.getRpmFan3Min());
		GaugeFanWaterPump4.setMaxValue(dashboard01DX.getRpmFan4Max());
		GaugeFanWaterPump4.setMinValue(dashboard01DX.getRpmFan4Min());
		GaugeFanAir.setMaxValue(dashboard01DX.getRpmFanAirMax());
		GaugeFanAir.setMinValue(dashboard01DX.getRpmFanAirMin());
		GaugeTemWater.setMaxValue(dashboard01DX.getTempExtMax());
		GaugeTemWater.setMinValue(dashboard01DX.getTempExtMin());
		GaugeTemInt.setMaxValue(dashboard01DX.getTempIntMax());
		GaugeTemInt.setMinValue(dashboard01DX.getTempIntMin());
		GaugePowerMonitor1.setMaxValue(dashboard01DX.getPwrTX1Max());
		GaugePowerMonitor1.setMinValue(dashboard01DX.getPwrTX1Min());
		GaugePowerMonitor2.setMaxValue(dashboard01DX.getPwrTX2Max());
		GaugePowerMonitor2.setMinValue(dashboard01DX.getPwrTX2Min());
		GaugePowerMonitor3.setMaxValue(dashboard01DX.getPwrTX3Max());
		GaugePowerMonitor3.setMinValue(dashboard01DX.getPwrTX3Min());
		GaugePowerMonitor4.setMaxValue(dashboard01DX.getPwrTX4Max());
		GaugePowerMonitor4.setMinValue(dashboard01DX.getPwrTX4Min());
		GaugePowerMonitor5.setMaxValue(dashboard01DX.getPwrTX5Max());
		GaugePowerMonitor5.setMinValue(dashboard01DX.getPwrTX5Min());
		GaugePowerMonitor6.setMaxValue(dashboard01DX.getPwrTX6Max());
		GaugePowerMonitor6.setMinValue(dashboard01DX.getPwrTX6Min());
		GaugeTemInt.setValueColor(Color.WHITE);
		GaugeTemWater.setValueColor(Color.WHITE);
		GaugeLCDAlim1.setValueColor(Color.WHITE);
		GaugeLCDAlim2.setValueColor(Color.WHITE);
		GaugeLCDAlim3.setValueColor(Color.WHITE);
		/*GaugePowerMonitor1.setValueColor(Color.WHITE);
		GaugePowerMonitor2.setValueColor(Color.WHITE);
		GaugePowerMonitor3.setValueColor(Color.WHITE);
		GaugePowerMonitor4.setValueColor(Color.WHITE);
		GaugePowerMonitor5.setValueColor(Color.WHITE);
		GaugePowerMonitor6.setValueColor(Color.WHITE);
		GaugeTempMonitor1.setValueColor(Color.WHITE);
		GaugeTempMonitor2.setValueColor(Color.WHITE);
		GaugeTempMonitor3.setValueColor(Color.WHITE);
		GaugeTempMonitor4.setValueColor(Color.WHITE);
		GaugeTempMonitor5.setValueColor(Color.WHITE);
		GaugeTempMonitor6.setValueColor(Color.WHITE);
		*/
		


	}

	private void setdashboard01SXMinMax() {

		D2GaugeWaterPump.setMaxValue(dashboard01SX.getRpmPmpMax());
		D2GaugeWaterPump.setMinValue(dashboard01SX.getRpmPmpMin());
		D2GaugeFanWaterPump1.setMaxValue(dashboard01SX.getRpmFan1Max());
		D2GaugeFanWaterPump1.setMinValue(dashboard01SX.getRpmFan1Min());
		D2GaugeFanWaterPump2.setMaxValue(dashboard01SX.getRpmFan2Max());
		D2GaugeFanWaterPump2.setMinValue(dashboard01SX.getRpmFan2Min());
		D2GaugeFanWaterPump3.setMaxValue(dashboard01SX.getRpmFan3Max());
		D2GaugeFanWaterPump3.setMinValue(dashboard01SX.getRpmFan3Min());
		D2GaugeFanWaterPump4.setMaxValue(dashboard01SX.getRpmFan4Max());
		D2GaugeFanWaterPump4.setMinValue(dashboard01SX.getRpmFan4Min());
		D2GaugeFanAir.setMaxValue(dashboard01SX.getRpmFanAirMax());
		D2GaugeFanAir.setMinValue(dashboard01SX.getRpmFanAirMin());
		D2GaugeTemWater.setMaxValue(dashboard01SX.getTempExtMax());
		D2GaugeTemWater.setMinValue(dashboard01SX.getTempExtMin());
		D2GaugeTemInt.setMaxValue(dashboard01SX.getTempIntMax());
		D2GaugeTemInt.setMinValue(dashboard01SX.getTempIntMin());
		D2GaugePowerMonitor1.setMaxValue(dashboard01SX.getPwrTX1Max());
		D2GaugePowerMonitor1.setMinValue(dashboard01SX.getPwrTX1Min());
		D2GaugePowerMonitor2.setMaxValue(dashboard01SX.getPwrTX2Max());
		D2GaugePowerMonitor2.setMinValue(dashboard01SX.getPwrTX2Min());
		D2GaugePowerMonitor3.setMaxValue(dashboard01SX.getPwrTX3Max());
		D2GaugePowerMonitor3.setMinValue(dashboard01SX.getPwrTX3Min());
		D2GaugePowerMonitor4.setMaxValue(dashboard01SX.getPwrTX4Max());
		D2GaugePowerMonitor4.setMinValue(dashboard01SX.getPwrTX4Min());
		D2GaugePowerMonitor5.setMaxValue(dashboard01SX.getPwrTX5Max());
		D2GaugePowerMonitor5.setMinValue(dashboard01SX.getPwrTX5Min());
		D2GaugePowerMonitor6.setMaxValue(dashboard01SX.getPwrTX6Max());
		D2GaugePowerMonitor6.setMinValue(dashboard01SX.getPwrTX6Min());
		D2GaugeTemInt.setValueColor(Color.WHITE);
		D2GaugeTemWater.setValueColor(Color.WHITE);
		D2GaugeLCDAlim1.setValueColor(Color.WHITE);
		D2GaugeLCDAlim2.setValueColor(Color.WHITE);
		D2GaugeLCDAlim3.setValueColor(Color.WHITE);
		
		
		/*D2GaugePowerMonitor1.setValueColor(Color.WHITE);
		D2GaugePowerMonitor2.setValueColor(Color.WHITE);
		D2GaugePowerMonitor3.setValueColor(Color.WHITE);
		D2GaugePowerMonitor4.setValueColor(Color.WHITE);
		D2GaugePowerMonitor5.setValueColor(Color.WHITE);
		D2GaugePowerMonitor6.setValueColor(Color.WHITE);
		D2GaugeTempMonitor1.setValueColor(Color.WHITE);
		D2GaugeTempMonitor2.setValueColor(Color.WHITE);
		D2GaugeTempMonitor3.setValueColor(Color.WHITE);
		D2GaugeTempMonitor4.setValueColor(Color.WHITE);
		D2GaugeTempMonitor5.setValueColor(Color.WHITE);
		D2GaugeTempMonitor6.setValueColor(Color.WHITE);
		*/

	}

	private void setListner() {
		// TODO Auto-generated method stub
		//Handler Dashboard 1
				
		
				SliderSwitchAmpl1.setOnMouseClicked((event) ->{
					if (stateSwitchAmpl1){
						SliderSwitchAmpl1.setValue(0);
						stateSwitchAmpl1=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/1", "off");
						GaugePowerMonitor1.setValueColor(Color.GRAY);
						GaugeTempMonitor1.setValueColor(Color.GRAY);
					}else{
						SliderSwitchAmpl1.setValue(100);
						stateSwitchAmpl1=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/1", "on");
						GaugePowerMonitor1.setValueColor(Color.WHITE);
						GaugeTempMonitor1.setValueColor(Color.WHITE);
					}
				});

				SliderSwitchAmpl2.setOnMouseClicked((event) ->{
					if (stateSwitchAmpl2){
						SliderSwitchAmpl2.setValue(0);
						stateSwitchAmpl2=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/2", "off");
						GaugePowerMonitor2.setValueColor(Color.GRAY);
						GaugeTempMonitor2.setValueColor(Color.GRAY);
					}else{
						SliderSwitchAmpl2.setValue(100);
						stateSwitchAmpl2=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/2", "on");
						GaugePowerMonitor2.setValueColor(Color.WHITE);
						GaugeTempMonitor2.setValueColor(Color.WHITE);
					}
				});
				SliderSwitchAmpl3.setOnMouseClicked((event) ->{
					if (stateSwitchAmpl3){
						SliderSwitchAmpl3.setValue(0);
						stateSwitchAmpl3=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/3", "off");
						GaugePowerMonitor3.setValueColor(Color.GRAY);
						GaugeTempMonitor3.setValueColor(Color.GRAY);
					}else{
						SliderSwitchAmpl3.setValue(100);
						stateSwitchAmpl3=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/3", "on");
						GaugePowerMonitor3.setValueColor(Color.WHITE);
						GaugeTempMonitor3.setValueColor(Color.WHITE);
					}
				});
				SliderSwitchAmpl4.setOnMouseClicked((event) ->{
					if (stateSwitchAmpl4){
						SliderSwitchAmpl4.setValue(0);
						stateSwitchAmpl4=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/4", "off");
						GaugePowerMonitor4.setValueColor(Color.GRAY);
						GaugeTempMonitor4.setValueColor(Color.GRAY);
					}else{
						SliderSwitchAmpl4.setValue(100);
						stateSwitchAmpl4=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/4", "on");
						GaugePowerMonitor4.setValueColor(Color.WHITE);
						GaugeTempMonitor4.setValueColor(Color.WHITE);
					}
				});
				SliderSwitchAmpl5.setOnMouseClicked((event) ->{
					if (stateSwitchAmpl5){
						SliderSwitchAmpl5.setValue(0);
						stateSwitchAmpl5=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/5", "off");
						GaugePowerMonitor5.setValueColor(Color.GRAY);
						GaugeTempMonitor5.setValueColor(Color.GRAY);
					}else{
						SliderSwitchAmpl5.setValue(1000);
						stateSwitchAmpl5=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/5", "on");
						GaugePowerMonitor5.setValueColor(Color.WHITE);
						GaugeTempMonitor5.setValueColor(Color.WHITE);
					}
				});
				SliderSwitchAmpl6.setOnMouseClicked((event) ->{
					if (stateSwitchAmpl6){
						SliderSwitchAmpl6.setValue(0);
						stateSwitchAmpl6=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/6", "off");
						GaugePowerMonitor6.setValueColor(Color.GRAY);
						GaugeTempMonitor6.setValueColor(Color.GRAY);
					}else{
						SliderSwitchAmpl6.setValue(100);
						stateSwitchAmpl6=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/dx/cmd/module/6", "on");
						GaugePowerMonitor6.setValueColor(Color.WHITE);
						GaugeTempMonitor6.setValueColor(Color.WHITE);
					}
				});

				//Handler Dashboard 2


				D2SliderSwitchAmpl1.setOnMouseClicked((event) ->{
					if (D2stateSwitchAmpl1){
						D2SliderSwitchAmpl1.setValue(0);
						D2stateSwitchAmpl1=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/1", "off");
						D2GaugePowerMonitor1.setValueColor(Color.GRAY);
						D2GaugeTempMonitor1.setValueColor(Color.GRAY);
						
					}else{
						D2SliderSwitchAmpl1.setValue(100);
						D2stateSwitchAmpl1=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/1", "on");
						D2GaugePowerMonitor1.setValueColor(Color.WHITE);
						D2GaugeTempMonitor1.setValueColor(Color.WHITE);
					}
				});

				D2SliderSwitchAmpl2.setOnMouseClicked((event) ->{
					if (D2stateSwitchAmpl2){
						D2SliderSwitchAmpl2.setValue(0);
						D2stateSwitchAmpl2=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/2", "off");
						D2GaugePowerMonitor2.setValueColor(Color.GRAY);
						D2GaugeTempMonitor2.setValueColor(Color.GRAY);
					}else{
						D2SliderSwitchAmpl2.setValue(100);
						D2stateSwitchAmpl2=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/2", "on");
						D2GaugePowerMonitor2.setValueColor(Color.WHITE);
						D2GaugeTempMonitor2.setValueColor(Color.WHITE);
					}
				});
				D2SliderSwitchAmpl3.setOnMouseClicked((event) ->{
					if (D2stateSwitchAmpl3){
						D2SliderSwitchAmpl3.setValue(0);
						D2stateSwitchAmpl3=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/3", "off");
						D2GaugePowerMonitor3.setValueColor(Color.GRAY);
						D2GaugeTempMonitor3.setValueColor(Color.GRAY);
					}else{
						D2SliderSwitchAmpl3.setValue(100);
						D2stateSwitchAmpl3=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/3", "on");
						D2GaugePowerMonitor3.setValueColor(Color.WHITE);
						D2GaugeTempMonitor3.setValueColor(Color.WHITE);
					}
				});
				D2SliderSwitchAmpl4.setOnMouseClicked((event) ->{
					if (D2stateSwitchAmpl4){
						D2SliderSwitchAmpl4.setValue(0);
						D2stateSwitchAmpl4=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/4", "off");
						D2GaugePowerMonitor4.setValueColor(Color.GRAY);
						D2GaugeTempMonitor4.setValueColor(Color.GRAY);
					}else{
						D2SliderSwitchAmpl4.setValue(100);
						D2stateSwitchAmpl4=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/4", "on");
						D2GaugePowerMonitor4.setValueColor(Color.WHITE);
						D2GaugeTempMonitor4.setValueColor(Color.WHITE);
					}
				});
				D2SliderSwitchAmpl5.setOnMouseClicked((event) ->{
					if (D2stateSwitchAmpl5){
						D2SliderSwitchAmpl5.setValue(0);
						D2stateSwitchAmpl5=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/5", "off");
						D2GaugePowerMonitor5.setValueColor(Color.GRAY);
						D2GaugeTempMonitor5.setValueColor(Color.GRAY);
					}else{
						D2SliderSwitchAmpl5.setValue(1000);
						D2stateSwitchAmpl5=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/5", "on");
						D2GaugePowerMonitor5.setValueColor(Color.WHITE);
						D2GaugeTempMonitor5.setValueColor(Color.WHITE);
					}
				});
				D2SliderSwitchAmpl6.setOnMouseClicked((event) ->{
					if (D2stateSwitchAmpl6){
						D2SliderSwitchAmpl6.setValue(0);
						D2stateSwitchAmpl6=false;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/6", "off");
						D2GaugePowerMonitor6.setValueColor(Color.GRAY);
						D2GaugeTempMonitor6.setValueColor(Color.GRAY);
					}else{
						D2SliderSwitchAmpl6.setValue(100);
						D2stateSwitchAmpl6=true;
						MessageBroker.getInstance().sendMessage(dashboard01DX.getShieldName()+"/sx/cmd/module/6", "on");
						D2GaugePowerMonitor6.setValueColor(Color.WHITE);
						D2GaugeTempMonitor6.setValueColor(Color.WHITE);
					}
				});

	}
	private void setColorGauge() {
		// TODO Auto-generated method stub
		GaugeLCDAlim1.setBarColor(Color.TRANSPARENT);
		GaugeLCDAlim2.setBarColor(Color.TRANSPARENT);
		GaugeLCDAlim3.setBarColor(Color.TRANSPARENT);
		GaugePowerMonitor1.setBarColor(Color.TRANSPARENT);
		GaugePowerMonitor2.setBarColor(Color.TRANSPARENT);
		GaugePowerMonitor3.setBarColor(Color.TRANSPARENT);
		GaugePowerMonitor4.setBarColor(Color.TRANSPARENT);
		GaugePowerMonitor5.setBarColor(Color.TRANSPARENT);
		GaugePowerMonitor6.setBarColor(Color.TRANSPARENT);
		GaugeTemInt.setBarColor(Color.TRANSPARENT);
		GaugeTempMonitor1.setBarColor(Color.TRANSPARENT);
		GaugeTempMonitor2.setBarColor(Color.TRANSPARENT);
		GaugeTempMonitor3.setBarColor(Color.TRANSPARENT);
		GaugeTempMonitor4.setBarColor(Color.TRANSPARENT);
		GaugeTempMonitor5.setBarColor(Color.TRANSPARENT);
		GaugeTempMonitor6.setBarColor(Color.TRANSPARENT);
		GaugeTemWater.setBarColor(Color.TRANSPARENT);
		D2GaugeLCDAlim1.setBarColor(Color.TRANSPARENT);
		D2GaugeLCDAlim2.setBarColor(Color.TRANSPARENT);
		D2GaugeLCDAlim3.setBarColor(Color.TRANSPARENT);
		D2GaugePowerMonitor1.setBarColor(Color.TRANSPARENT);
		D2GaugePowerMonitor2.setBarColor(Color.TRANSPARENT);
		D2GaugePowerMonitor3.setBarColor(Color.TRANSPARENT);
		D2GaugePowerMonitor4.setBarColor(Color.TRANSPARENT);
		D2GaugePowerMonitor5.setBarColor(Color.TRANSPARENT);
		D2GaugePowerMonitor6.setBarColor(Color.TRANSPARENT);
		D2GaugeTemInt.setBarColor(Color.TRANSPARENT);
		D2GaugeTempMonitor1.setBarColor(Color.TRANSPARENT);
		D2GaugeTempMonitor2.setBarColor(Color.TRANSPARENT);
		D2GaugeTempMonitor3.setBarColor(Color.TRANSPARENT);
		D2GaugeTempMonitor4.setBarColor(Color.TRANSPARENT);
		D2GaugeTempMonitor5.setBarColor(Color.TRANSPARENT);
		D2GaugeTempMonitor6.setBarColor(Color.TRANSPARENT);
		D2GaugeTemWater.setBarColor(Color.TRANSPARENT);

	}
	/*public Gauge getGauge(){
		return this.GaugeFanWaterPump1;
	}*/

	public void setContext(Main main) {
		mContext = main;
	}

}