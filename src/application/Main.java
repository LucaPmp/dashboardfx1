package application;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import deserializer.configDeserializer;

//import deserializer.configDeserializer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Config;

/**
 * Main class to start the application.
 *
 * @author Marco Jakob
 */
public class Main extends Application {

	Stage mStage;
	FXMLLoader loader;
	private BorderPane page;
	public String currUser;
	private Stage dialogStage;
	public static Config config;

	@Override
	public void start(Stage primaryStage) {

		config = configDeserializer.config("config");
		primaryStage.setTitle("Dashboard Ver: 1.0");

		mStage= primaryStage;

		//PropertyFileHelper props = PropertyFileHelper.instance();


		try {
			FXMLLoader loader2 = new FXMLLoader(Main.class.getResource("login.fxml"));
			loader = new FXMLLoader(Main.class.getResource("dashNew.fxml"));
			Pane page2 = (Pane) loader2.load();

			//

			((LoginController)loader2.getController()).setContext(this);
			
			

			Scene scene2 = new Scene(page2);

//			FXMLLoader loader = new FXMLLoader(Main.class.getResource("dash.fxml"));
//			BorderPane page = (BorderPane) loader.load();
//			Scene scene = new Scene(page);
			primaryStage.setScene(scene2);

			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		launch(args);
	}


	
	
	public void goToMain(String user)
	{
		try {
			page = (BorderPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((DashController)loader.getController()).setContext(this);
		Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                if(!MessageReceiver.isConnected()){
                	DashController.dashboardStart();
                }
            }
        }, 0, 30000);

			currUser = user; 
			Scene scene = new Scene(page);
			mStage.setScene(scene);
			mStage.centerOnScreen();
				}

	public void showWarning(String text, boolean hideDialog) {
		final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(mStage);
        VBox dialogVbox = new VBox(20);
        dialogVbox.getChildren().add(new Text(text));
        Scene dialogScene = new Scene(dialogVbox, 200, 100);
        dialog.setScene(dialogScene);
        dialog.show();
        
        if(hideDialog&&dialogStage!=null)
        	dialogStage.close();
        

	}

	public void showChangePassDialog() {
		FXMLLoader loader2 = new FXMLLoader(Main.class.getResource("changePass.fxml"));


		Pane page2;
		try {
			page2 = (Pane) loader2.load();
			Scene scene2 = new Scene(page2);

			((ChangePassController)loader2.getController()).setContext(this);


			dialogStage = new Stage();
			dialogStage.initModality(Modality.WINDOW_MODAL);

			dialogStage.setScene(scene2);
			dialogStage.show();
			
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
}
