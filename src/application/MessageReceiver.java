package application;

import java.util.HashMap;
import java.util.concurrent.locks.Condition;

import javax.naming.Context;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import deserializer.configDeserializer;
import eu.hansolo.medusa.Gauge;
import javafx.scene.shape.Circle;
import model.Config;

class MessageReceiver implements Runnable
{

	private String mTopic="";
	private HashMap<String, Object> mContext;
	private static MqttClient client;


	public MessageReceiver(String topic, HashMap<String, Object> mContext)
	{
		this.mTopic = topic;
		this.mContext = mContext;


	}

	@Override
	public void run() {
			System.out.println("== START SUBSCRIBER ==");

		    
		try {
			MqttConnectOptions connectOptions = new MqttConnectOptions();
			connectOptions.setKeepAliveInterval(5);
			
			client = new MqttClient("tcp://"+Main.config.getMQTTBrokerIp()+":"+Main.config.getMQTTBrokerPort(), MqttClient.generateClientId());
			
		    client.setCallback( new SimpleMqttCallBack(mContext) );
		    
		    client.connect(connectOptions);

		    client.subscribe(mTopic);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			
		}

	}
	
	public static int stopClient(){
		
		
		try {
			client.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		return 1;
		
	}
	
	public static boolean isConnected(){
		
		return client.isConnected();
	}

}