package application;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;



public class MessageBroker {
	   private static MessageBroker instance = null;
	   protected MessageBroker() {

	   }

	   public static MessageBroker getInstance() {
	      if(instance == null) {
	         instance = new MessageBroker();
	      }
	      return instance;
	   }

	   public void sendMessage(String topic, String text)
	   {
		   		MqttClient client;
		   try {
				client = new MqttClient("tcp://"+Main.config.getMQTTBrokerIp()+":"+Main.config.getMQTTBrokerPort(), MqttClient.generateClientId());
				
			    client.connect();
			    MqttMessage message = new MqttMessage();
			    message.setPayload(text.getBytes());
			    client.publish(topic, message);
			    client.messageArrivedComplete(message.getId(), 1);

			    System.out.println(topic+text);

			    client.disconnect();

			    System.out.println("== END PUBLISHER ==");
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }


	}