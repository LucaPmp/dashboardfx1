package application;

import eu.hansolo.medusa.Gauge;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * View-Controller for the person table.
 *
 * @author Marco Jakob
 */
public class LoginController {

	@FXML
	private Button loginBtn;
	@FXML
	private TextField userTxt;
	@FXML
	private TextField passwordTxt;


	private Main mContext;


	/**
	 * The constructor (is called before the initialize()-method).
	 */
	public LoginController() {

	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {







		loginBtn.setOnMouseClicked((event) -> {

			
			if(! PasswordStore.instance().login(userTxt.getText().trim(), passwordTxt.getText().trim()) )
				((Main)mContext).showWarning("Wrong User or Password!",false);

			else

				((Main)mContext).goToMain(userTxt.getText().trim());
					

		});



	}



	public void setContext(Main main) {
		mContext = main;

	}


}