package application;



import java.io.*;
import java.lang.annotation.Documented;
import java.util.Properties;

public class PropertyFileHelper {
    static private PropertyFileHelper _instance = null;
    static public String port = null;
    static public String database = null;
    static public String ip = null;
    static public String user = null;
    static public String pass = null;
    static public String jdbc = null;
    static public String driver = null;
    static public String instance = null;

    protected PropertyFileHelper(){
    try{
        InputStream file = new FileInputStream(new File("config.prop")) ;
        Properties props = new Properties();
        props.load(file);
        port = props.getProperty("PORT");
        ip = props.getProperty("IP");
        database = props.getProperty("DATABASE");
        user = props.getProperty("USER");
        pass = props.getProperty("PASS");
        jdbc = props.getProperty("JDBC");
        driver = props.getProperty("DRIVERNAME");
        instance = props.getProperty("INSTANCE");
       }
    catch(Exception e){
        System.out.println("error" + e);
       }
    }

    static public PropertyFileHelper instance(){
        if (_instance == null) {
            _instance = new PropertyFileHelper();
        }
        return _instance;
    }

    /**
	 * @setProperties: fileName without extension
	 */
    static public void setProperties(String fileName, String key, String value){
    	InputStream file;
    	
		try {
			file = new FileInputStream(new File("src\\propertis\\"+fileName+".prop"));
			Properties props = new Properties();
	        props.load(file);
	        props.setProperty(key,value);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    /**
	 * @getProperties: fileName without extension
	 */
    static public String getProperties(String fileName, String key){
    	InputStream file;
    	String value;
		try {
			file = new FileInputStream(new File("src\\propertis\\"+fileName+".prop"));
			Properties props = new Properties();
	        props.load(file);
	        value = props.getProperty(key);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			value="";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			value="";
		}
		return value;
    }

}