package application;

import java.util.HashMap;

import javax.swing.text.AbstractDocument.Content;

import eu.hansolo.medusa.Gauge;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;


/**
 * View-Controller for the person table.
 *
 * @author Marco Jakob
 */
public class ChangePassController {

	
	
	@FXML
	private TextField newPassTxt;
	
	@FXML
	private TextField repeatPassTxt;
	
	@FXML
	private TextField currPassTxt;

	private Main mContext;
	
	
	
	@FXML
	private void actionChangePwd(ActionEvent event) {
		if(!PasswordStore.instance().getUserPwd(mContext.currUser).equals(currPassTxt.getText().trim()))
			((Main)mContext).showWarning("Current Password doesn't match!",false);
		else if( !newPassTxt.getText().trim().equals(repeatPassTxt.getText().trim()))
			((Main)mContext).showWarning("New Password and Repeat New Password doesn't match!",false);
		else
		{
			PasswordStore.instance().setNewPassword(mContext.currUser,newPassTxt.getText().trim());
			((Main)mContext).showWarning("Change Password Success!",true);
		}
	}
	
	


	/**
	 * The constructor (is called before the initialize()-method).
	 */
	public ChangePassController() {

	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		

	}
	
	public void setContext(Main main) {
		mContext = main;
		
	}

}